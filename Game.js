let nCurrent = 0;
var firstPlayer = 0;
var secondPlayer = 0;

module.exports = class Game {
    constructor() {
        this.nCurrent = 0;
    }

    makeAMove(sInput) {
        let aReturn = [];
        if (this.nCurrent == 0) {
            aReturn.push("Welcome to Riddle Game");
            aReturn.push("It is a Multiplayer Game");
            aReturn.push("Are You Ready ?");
            aReturn.push("Type Yes / No ");
            this.nCurrent = 1;
            return aReturn;
        }
        if (this.nCurrent == 1) {
            if (sInput.toLowerCase().match("yes")) {
                aReturn.push("Player 1 Turn");
                aReturn.push("At night they come without being fetched. By day they are lost without being stolen.What are they")
                this.nCurrent = 2;
                return aReturn;
            }
            else {
                aReturn.push("See you next time ");
                return aReturn;
            }
        }

        if (this.nCurrent == 2) {
            if (sInput.toLowerCase().match("stars") || sInput.toLowerCase().match("star")) {
                aReturn.push("Correct");
                aReturn.push("Player 2 Turn");
                aReturn.push("I am not alive, but I grow: I don't have lungs,but I need air,I don't have mouth but water kills me. What am I");
                firstPlayer += 1;
                this.nCurrent = 3;
                return aReturn;

            } else {
                aReturn.push("Wrong done");
                aReturn.push("Player 2 Turn");
                aReturn.push("I am not alive, but I grow: I don't have lungs,but I need air,I don't have mouth but water kills me. What am I");
                this.nCurrent = 3;
                return aReturn;
            }
        }

        if (this.nCurrent == 3) {
            if (sInput.toLowerCase().match("fire")) {
                aReturn.push("Correct");
                aReturn.push("Player 1 Turn");
                aReturn.push("I only point in one direction but i guide people around the world . What am I ?");
                secondPlayer += 1;
                this.nCurrent = 4;
                return aReturn;
            } else {
                aReturn.push("Wrong");
                aReturn.push("Player 1 Turn");
                aReturn.push("I only point in one direction but i guide people around the world . What am I ?");
                this.nCurrent = 4;
                return aReturn;
            }
        }

        if (this.nCurrent == 4) {
            if (sInput.toLowerCase().match("compass")) {
                aReturn.push("Correct");
                aReturn.push("Player 2 Turn");
                aReturn.push("All men take off their hats to one person. Who is this person");
                firstPlayer += 1;
                this.nCurrent = 5;
                return aReturn;
            } else {
                aReturn.push("Wrong");
                aReturn.push("Player 2 Turn");
                aReturn.push("All men take off their hats to one person. Who is this person");
                this.nCurrent = 5;
                return aReturn;
            }
        }

        if (this.nCurrent == 5) {
            if (sInput.toLowerCase().match("barber")) {
                aReturn.push("Correct");
                aReturn.push("Player 1 Turn");
                aReturn.push("I can fly but I have no wings, i can cry but I have no eyes and wherever I go darkness follows me . What am I");
                secondPlayer += 1;
                this.nCurrent = 6;
                return aReturn;
            } else {
                aReturn.push("Wrong");
                aReturn.push("Player 1 Turn");
                aReturn.push("I can fly but I have no wings, i can cry but I have no eyes and wherever I go darkness follows me . What am I");
                this.nCurrent = 6;
                return aReturn;
            }
        }

        if (this.nCurrent == 6) {
            if (sInput.toLowerCase().match("clouds")) {
                aReturn.push("Correct");
                aReturn.push("Do You want to continue the Game. Yes / No ");
                firstPlayer += 1;
                this.nCurrent = 7;
                return aReturn;
            } else {
                aReturn.push("Wrong");
                aReturn.push("Do You want to continue the Game. Yes / No ");
                this.nCurrent = 7;
                return aReturn;
            }
        }

        if (this.nCurrent == 7) {
            if (sInput.toLowerCase().match("yes")) {
                aReturn.push("Under Development")
                if (firstPlayer == secondPlayer) {
                    aReturn.push("both tied");
                    aReturn.push("Thank you for playing ");
                    return aReturn;
                }
                else if (firstPlayer > secondPlayer) {
                    aReturn.push("first player wins");
                    aReturn.push("Thank you for playing ");
                    return aReturn;
                } else {
                    aReturn.push("second player wins");
                    aReturn.push("Thank you for playing ");
                    return aReturn;
                }
            }
            else if (sInput.toLowerCase().match("no")) {
                if (firstPlayer == secondPlayer) {
                    aReturn.push("both tied");
                    return aReturn;
                }
                else if (firstPlayer > secondPlayer) {
                    aReturn.push("first player wins");
                    return aReturn;
                } else {
                    aReturn.push("second player wins");
                    return aReturn;
                }
                
            }
        }

    }
}