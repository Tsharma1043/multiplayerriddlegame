*******************************************************************************

Hello 

My name is Tushar Sharma  
1. I have created a public repository in which i have uploaded my Project which is Multiplayer Riddle Game.
2. I have taken MIT licensed which is free.
3. I have created a Wiki File that contain the Information about Node.js 


## Overview

This is basic Multiplayer Riddle Game created in Node.js and hosted on Heroku so that anyone can play the game.
It is single localhost game which means that it can be played on one localhost 
not on 2 localhost. The game is not completed yet and Developer(me) still working on it .
 When you launch the application through Heroku then first you have to type 'Hello' or anything to start the Game. As the Game
state is changes when user enter the input first and based on that outcome is generated. 

## Minimum Requirements
1. You need to have Any Browser Install on your System if you running this application on Heroku and stable internet Connection.
2. If you want to play on Localhost and without internet then you have to install Node.js on your system to run it.
3. There should at least on code Editor to run the file . I prefer Visual Studio Code as it is easy to work on the code and its seemless integration
   help the user to run the file sing its terminal.

## Things You have to Download
1. RiddleGame folder and save it on your Desktop 
2. Download latest Node.js and instal it on you system 

## How to Run this File 

1. Open Visual Studio Code
2. Go to File and Open Project and search for RiddleGame that you hav downloaded 
3. Open Visual Studio Terminal through the Terminal Tab Option or Press (ctrl + ~) and Type install node which will install node module in that folder
4. Once the Node is install, run the file through terminal by typing start index.js 
5. you will see that your default internet explorer will open up and will show the Game  

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

*******************************************************************************